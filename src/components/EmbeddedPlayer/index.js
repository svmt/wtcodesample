import React from "react";

export const EmbeddedPlayer = () => {
	return (
		<>
			<iframe
				width='100%'
				height='100%'
				src='https://www.youtube.com/embed/YGE5euSZnbI?autoplay=1&controls=0&disablekb=1&loop=1&playlist=YGE5euSZnbI&modestbranding=1'
				frameborder='0'
				allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
				allowfullscreen
			></iframe>
		</>
	);
};

export default EmbeddedPlayer;
