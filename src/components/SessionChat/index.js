import React,{ useState, useEffect } from 'react';
import './index.css'

export const SessionChat = ({WT}) => {
  const [messages, setMessages] = useState([])
  
  useEffect( () => {
		
    WT.SessionListeners.onMessageReceived(({message, user}) => {
      setMessages((messages) => [
				...messages,
				{user, message, time: setMessageTime()}
			])
    })

    WT.SessionListeners.onMessageDelivered( ({message, user}) => {
      setMessages((messages) => [
				...messages,
				{ user: "local", message, time: setMessageTime() },
			]);
    })

    return () => {
    }
	}, [])
	
	const setMessageTime = () => {
		const now = new Date();
		const hours = now.getHours() >= 10 ? now.getHours() : `0${now.getHours()}`;
		const minutes = now.getMinutes() >= 10 ? now.getMinutes() : `0${now.getMinutes()}`;

		return `${hours}:${minutes}`
	}

  const handleKeyPress = (event) => {
		if (event.key === "Enter") {
      WT.Session.sendMessage(event.target.value)
      event.target.value = ''
		}
	};


  return (
		<>
			<div className='message-container'>
				<div className='messages'>
					{messages
						? messages.map((msgObj, i) => {
								return (
									<div
										style={{
											justifyContent:
												msgObj.user === "local" ? "flex-end" : "flex-start",
										}}
										key={`${msgObj.user + i}`}
									>
										<span
											style={{
												background:
													msgObj.user === "local"
														? "rgba(144, 144, 144, 0.3)"
														: "rgba(1,1,1,0.3)",
											}}
										>
											{msgObj.message}
											<span className='message-time'>{msgObj.time}</span>
										</span>
									</div>
								);
						  })
						: null}
				</div>
				<input
					className='chat-input'
					placeholder='Enter text message'
					onKeyPress={handleKeyPress}
				/>
			</div>
		</>
	);
}