import React, {useState, useEffect} from 'react';
import EmbeddedPlayer from "../EmbeddedPlayer";
import VideoContainer from '../VideoContainer';
import { useHistory } from "react-router-dom";
import {apiRequest} from '../../heplers/api';
import WT from '@sscale/wtsdk';
import {SessionChat} from '../SessionChat';
import './Session.css';

/**
 * Watch Together Session
 *
 * WT.Session.onConnected - Method for setting up session when it ready
 * @param {onConnectedCallback} callback - Callback fired when session is ready
 * @callback onConnectedCallback
 * @param {Array} Participants - callback function contain an array of participants in the session
 *
 * WT.SessionListeners.onStreamCreated - Metod will allow you get information about new participants
 * @param {onStreamCreated} callback - Callback fired everytime when participant stream is reade
 * @callback onStreamCreated
 * @param {Object} ParticipantInfo - callback function contain object with participant information
 *
 * WT.ParticipantListeners.onParticipantLeft - Method will allow you get information about participant which left the session
 * @param {onParticipantLeftCallback} callback - Callback fired everytime when participant leave from the session
 * @callback onParticipantLeftCallback
 * @param {String} ParticipantName
 *
 * WT.Session.disconnect - Method will allow you disconnecting from the Session
 *
 * WT.Session.connect - Method will allow you connecting to the Session
 * @param {Object} SessionSettings - Information for creation the session
 * @param SessionSettings.sessionToken - session token
 * @param SessionSettings.wsUrl - WebSocket Url
 * @param SessionSettings.sessionId - The identifier of the session
 * @param {Object} UserInformation - Information about user
 * @param UserInformation.displayed_name - User ID
 *
 * WT.ErrorListeners.onSessionError - Method will allow you handling session errors
 * @param {onSesionErrorCallback} callback - Callbeck fired everytime when arises error with session
 * @callback onSesionErrorCallback
 * @param {Object} ErrorObject - Information about sesion error
 * @param code - error code
 * @param message - error message
 * @param requestTime - error time in unix timestamp
 */

const Session = ({location}) => {
    const [participants, setParticipants] = useState([]);
    const [isCopied, setIsCopied] = useState(false);
    const [isChatOpen, setIsChatOpen] = useState(false)
    const [isMuted, setIsMuted] = useState(false);
    const [isVideoEnabled, setIsVideoEnabled] = useState(false)
    const [isConnected, setIsConnected] = useState(false)
    const history = useHistory()
    let sessionId = location && location.state ? location.state.sessionId : null;
    const videoContainerWidth = {
        4: '21%',
        5: '16%',
        6: '13%',
        7: '12%',
    }

    useEffect(() => {
        window.onpopstate = () => {
			WT.Session.disconnect();
		};
        if(!location.state && !history.location.search) {
            return history.push('/')
        }

        if(!location.state && history.location.search) {
            sessionId = history.location.search.replace(/^.*?\=/, "");
        }

        WT.ErrorListeners.onSessionError((event) => {
            if (event.error.code === 206) {
                // handle on session full error
            }
        });

        WT.SessionListeners.onConnected(() => {
            // Calls when new streams added
            WT.SessionListeners.onStreamCreated((stream) => {
                const { streams, participant, local, participantId } = stream;

                setParticipants((participants) =>[
                    ...participants,
                    {
                        stream: streams[0],
                        participant,
                        participantId,
                        isLocal: local,
                    },
                ]);
            });

            WT.ParticipantListeners.onParticipantLeft((participantId) => {
                setParticipants((participants) =>
                    participants.filter((p) => p.participantId !== participantId)
                );
            });
            setIsConnected(true);
        });

        authProcess().then(({ msc_url: wsUrl, token }) => {
            WT.Session.connect(
                {
                    sessionToken: token,
                    wsUrl,
                    sessionId,
                },
                { displayed_name: "" }
            );
        });

        return () => {
        };
    }, []);

    const authProcess = async () => {
        try {
            const loginResponse = await apiRequest.post(`/login`, {
                login: process.env.REACT_APP_CLUSTER_LOGIN || 'your auth login',
                password: process.env.REACT_APP_CLUSTER_PASSWORD || 'your auth password',
            });

            const {token: loginToken} = loginResponse.data;

            const tokenResponse = await apiRequest.get(`/token?token=${loginToken}`);

            const {msc_url, token} = tokenResponse.data;
            return { msc_url, token };
        } catch (error) {
            console.log("error ", error);
        }
    };

    const handleInviteFriend = () => {
        //copy url to clipboard
        const elem = document.createElement("textarea");
        elem.value = location.search
            ? window.location.href
            : `${window.location.href}?sessionId=${sessionId}`;
        document.body.appendChild(elem);
        elem.select();
        document.execCommand("copy");
        document.body.removeChild(elem);

        setIsCopied(true);
        setTimeout(() => setIsCopied(false), 2000);
    };

    const toggleAudio = () => {
        if (WT.Participant.isAudioEnabled()) {
            WT.Participant.disableAudio();
            setIsMuted(true)
        } else {
            WT.Participant.enableAudio();
            setIsMuted(false)
        }
    };

    const toggleVideo = () => {
        if (WT.Participant.isVideoEnabled()) {
            WT.Participant.disableVideo();
            setIsVideoEnabled(true)
        } else {
            WT.Participant.enableVideo();
            setIsVideoEnabled(false);
        }
    };

    const toggleChat = () => {
        setIsChatOpen(!isChatOpen)
    };

    return (
        <>
            <div className='main-container'>
                <div className='open-chat' onClick={toggleChat}>
                    <i className='zmdi zmdi-comment-outline'></i>
                </div>

                <div className='main-container_header'>
                    <span>Powered by </span>
                    <div className='header__icon'></div>
                </div>
                <div className='content-wrapper'>
                    <div
                        className='chat-wrapper'
                        style={{
                            display: isChatOpen ? "flex" : "none",
                        }}
                    >
                        <div className='chat-container'>
                            {isConnected ? <SessionChat WT={WT} /> : null}
                        </div>
                    </div>
                    <div className='iframe-container'>
                        <EmbeddedPlayer/>
                    </div>
                    <div
                        className='streams-container'
                        style={{
                            width: `${
                                participants.length > 4
                                    ? videoContainerWidth[participants.length]
                                    : "20%"
                            }`,
                        }}
                    >
                        <div
                            className='participants'
                            style={{
                                height: `${
                                    participants.length < 4 ? "calc(100% - 36px)" : "100%"
                                }`,
                            }}
                        >
                            {participants.length
                                ? participants.sort(({ isLocal }) => (isLocal ? -1 : 1)) &&
                                    participants.map((user, index) => {
                                        return user.stream ? (
                                            <VideoContainer
                                                key={user.participantId}
                                                stream={user.stream}
                                                participant={user.participant}
                                                isLocal={user.isLocal}
                                                isMuted={user.isLocal ? isMuted : null}
                                                isVideoEnabled={user.isLocal ? isVideoEnabled : null}
                                                toggleAudio={toggleAudio}
                                                toggleVideo={toggleVideo}
                                            />
                                        ) : null;
                                    })
                                : null}
                        </div>
                        {participants.length < 4 ? (
                            <div className='invite-button' onClick={handleInviteFriend}>
                                <span>Invite friends</span>
                            </div>
                        ) : (
                            ""
                        )}
                    </div>
                </div>
                {isCopied ? (
                    <div className='popup-message'>Copied to clipboard</div>
                ) : (
                    ""
                )}
            </div>
        </>
    );
};

export default Session;
