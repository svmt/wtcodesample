import React, { useState, useEffect } from "react";
import "material-design-iconic-font/dist/css/material-design-iconic-font.min.css";
import { useHistory } from "react-router-dom";

import "./index.scss";

const MainPage = () => {
	const [actualTime, setActualTime] = useState(null);
	const history = useHistory();

	useEffect(() => {
		const updateTime = setInterval(() => {
			const now = new Date();
			const hours =
				now.getHours() >= 10 ? now.getHours() : `0${now.getHours()}`;
			const minutes =
				now.getMinutes() >= 10 ? now.getMinutes() : `0${now.getMinutes()}`;

			setActualTime(`${hours}:${minutes}`);
		}, 1000);

		return () => {
			clearInterval(updateTime);
		};
	}, []);

	const handleRedirect = () => {
		history.push("/session", {
			sessionId: [...Array(32)]
				.map((i) => (~~(Math.random() * 36)).toString(36))
				.join(""),
		});
	};

	return (
		<div className='menu-wrapper'>
			<div className='background-image'/>
			<div className='bottom-panel'>
				<div className='bottom-container'>
					<div className='panel-header'>
						<div className='xfinity-logo' />
						<div className='panel-time'>{actualTime}</div>
					</div>
					<div className='panel-menu'>
						<div className='speach-icon' />
						<div>Guide</div>
						<div>Saved</div>
						<div>On Demand</div>
						<div>Search</div>
						<div onClick={handleRedirect}>Watch Together</div>
						<div className='settings-icon' />
					</div>
					<div className='news-container flex justify-between'>
						<div className='first-news' />
						<div className='second-news' />
						<div className='third-news' />
						<div className='fourth-news' />
					</div>
				</div>
			</div>
		</div>
	);
};

export default MainPage;