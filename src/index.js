import React from 'react';
import ReactDOM from 'react-dom';
import MainPage from './components/MainPage';
import Session from './components/Session/Session';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './index.css';

ReactDOM.render(
	<Router>
		<Route path='/' exact component={MainPage}></Route>
		<Route path='/session' component={Session}></Route>
	</Router>,
	document.getElementById("root")
);
