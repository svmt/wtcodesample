import axios from 'axios';

export const apiRequest = axios.create({
    baseURL: `https://${process.env.REACT_APP_CLUSTER_AUTH}.sceenic.co`, // your auth url
    headers: {
        'Content-Type': 'application/json'
    }
});
