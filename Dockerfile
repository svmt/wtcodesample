FROM node:10.17-alpine
LABEL version="1.0"
ARG REACT_APP_CLUSTER_AUTH
ARG REACT_APP_CLUSTER_LOGIN
ARG REACT_APP_CLUSTER_PASSWORD
ARG NPM_TOKEN
WORKDIR /app
COPY . .
RUN echo "//registry.npmjs.org/:_authToken=$NPM_TOKEN" > .npmrc
RUN npm i
RUN env
RUN npm run build

FROM nginx:1.17
COPY --from=0 /app/build /usr/share/nginx/html
